# Ares

Ares is a game about controlling a Martian robot remotely from Earth to
try to escape from the police robots.

To launch it, run `python3 run_game.py`.

The in-game tutorial explains how to play. You can try things out as you
read it.

## Dependencies

I recommend Python 3.8.0+ and Pygame 2.0.1, but it should be compatible
with Pygame 1.8.1+.

## Copyright

I wrote all of the code, drew the artwork, and generated the sound
effects (using [jsfxr](sfxr.me)) myself. The font is [Generic Pixel Font
5x7
Neue](https://fontstruct.com/fontstructions/show/1625754/generic-pixel-font-5x7-neue),
which is licenced under the SIL Open Font License. See `ares/data/font/`
for details.

---

Ares
Copyright (C) 2021 Tom Fryers

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
