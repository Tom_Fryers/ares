from __future__ import annotations

from random import randint

# These lies keep the pixels lined up
SQRT3 = 12 / 7
SQRT32 = 7 / 6


def random(radius):
    while True:
        pos = Vec(x := randint(-radius, radius), y := randint(-radius, radius), -x - y)
        if abs(pos) <= radius:
            return pos


class Vec:
    __slots__ = ["x", "y", "z"]
    x: int
    y: int
    z: int

    def __init__(self, x, y, z):
        if x + y + z != 0:
            raise ValueError("x + y + z must be zero")
        object.__setattr__(self, "x", x)
        object.__setattr__(self, "y", y)
        object.__setattr__(self, "z", z)

    def __setattr__(self, *_):
        raise TypeError(f"{self.__class__.__qualname__} is immutable")

    def __eq__(self, other: Vec):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y, self.z))

    def __add__(self, other: Vec):
        return Vec(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other: Vec):
        return Vec(self.x - other.x, self.y - other.y, self.z - other.z)

    def __neg__(self):
        return Vec(-self.x, -self.y, -self.z)

    def __mul__(self, scalar: int):
        return Vec(self.x * scalar, self.y * scalar, self.z * scalar)

    def __truediv__(self, scalar: int):
        if any(a % scalar for a in (self.x, self.y, self.z)):
            raise ValueError(f"{self} is not divisible by {scalar}")
        return Vec(self.x // scalar, self.y // scalar, self.z // scalar)

    def __str__(self):
        return f"({self.x} {self.y} {self.z})"

    def __repr__(self):
        return f"{self.__class__.__qualname__}({self.x}, {self.y}, {self.z})"

    def __bool__(self):
        return self.x != 0 and self.y != 0

    def __abs__(self):
        return max(abs(a) for a in self.to_tuple())

    def cartesian(self, size=1):
        return (size * (self.x - self.y), size * 2 * self.z)

    def to_tuple(self):
        return (self.x, self.y, self.z)


class VecF:
    __slots__ = ["x", "y"]
    x: float
    y: float

    def __init__(self, x, y=None):
        if y is None:
            y = x.y
            x = x.x

        object.__setattr__(self, "x", x)
        object.__setattr__(self, "y", y)

    @property
    def z(self):
        return -self.x - self.y

    def __setattr__(self, *_):
        raise TypeError(f"{self.__class__.__qualname__} is immutable")

    def __eq__(self, other: Vec):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def __add__(self, other: VecF):
        return VecF(self.x + other.x, self.y + other.y)

    def __sub__(self, other: VecF):
        return VecF(self.x - other.x, self.y - other.y)

    def __neg__(self):
        return VecF(-self.x, -self.y)

    def __mul__(self, scalar: float):
        return VecF(self.x * scalar, self.y * scalar)

    def __truediv__(self, scalar: float):
        return VecF(self.x / scalar, self.y / scalar)

    def __str__(self):
        return f"({self.x} {self.y} {self.z})"

    def __repr__(self):
        return f"{self.__class__.__qualname__}({self.x}, {self.y})"

    def __bool__(self):
        return self.x != 0 and self.y != 0

    def __abs__(self):
        return max(abs(a) for a in self.to_tuple())

    def cartesian(self, size=1):
        return (size * (self.x - self.y), size * 2 * self.z)

    def to_tuple(self):
        return (self.x, self.y, self.z)


class Grid:
    def __init__(self, radius: int):
        self.radius = radius
        self.grid = []
        self.positions = []
        for x in range(-radius, radius + 1):
            z_count = 2 * radius + 1 - abs(x)
            self.grid.append([None for _ in range(z_count)])
            for z in range(-radius, radius + 1):
                pos = Vec(x, -x - z, z)
                if abs(pos) <= radius:
                    self.positions.append(pos)

    def __getitem__(self, position: Vec):
        return self.grid[position.x + self.radius][
            position.z + min(position.x, 0) + self.radius
        ]

    def __setitem__(self, position: Vec, value):
        self.grid[position.x + self.radius][
            position.z + min(position.x, 0) + self.radius
        ] = value

    def __repr__(self):
        return f"<{self.__class__.__qualname__} grid={self.grid!r}"


EA = Vec(1, -1, 0)
NE = Vec(1, 0, -1)
NW = Vec(0, 1, -1)
WE = Vec(-1, 1, 0)
SW = Vec(-1, 0, 1)
SE = Vec(0, -1, 1)

DIRECTIONS = (EA, NE, NW, WE, SW, SE)

DIRECTION_NAMES = ("ea", "ne", "nw", "we", "sw", "se")
