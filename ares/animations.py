import math

import pygame

from . import colours, hexes


def lerp(initial, final, progress):
    return initial * (1 - progress) + final * progress


pygame.mixer.init()

BEAM_SOUND = pygame.mixer.Sound("ares/data/sounds/laser.wav")
HIT_SOUND = pygame.mixer.Sound("ares/data/sounds/hit.wav")
DEATH_SOUND = pygame.mixer.Sound("ares/data/sounds/death.wav")
SIREN_SOUND = pygame.mixer.Sound("ares/data/sounds/siren.wav")


class Animation:
    def __init__(self, thing, duration):
        self.thing = thing
        self.progress = 0
        self.duration = duration

    def lerp(self, initial, final):
        return lerp(initial, final, self.progress)

    def start(self):
        pass


class Translation(Animation):
    def __init__(self, thing, old_pos, duration=0.2):
        super().__init__(thing, duration)
        self.old_pos = old_pos

    def position(self, new_pos):
        return self.lerp(hexes.VecF(self.old_pos), hexes.VecF(new_pos))


class Jump(Translation):
    def sprite(self, sprite):
        new = pygame.Surface(sprite.get_size(), pygame.SRCALPHA)
        new_size = [
            round(x / (1 - 0.9 * (self.progress) * (1 - self.progress)))
            for x in sprite.get_size()
        ]
        new.blit(
            pygame.transform.scale(sprite, new_size),
            [(o - n) / 2 for o, n in zip(sprite.get_size(), new_size)],
        )
        return new


class Rotation(Animation):
    def __init__(self, thing, old_rot, duration=0.2):
        super().__init__(thing, duration)
        self.old_rot = old_rot

    def rotation(self, new_rot):
        return ((((self.old_rot - new_rot) + 2) % 6) - 2) * (1 - self.progress) * 60

    def sprite(self, sprite, entity):
        sprite = sprite.copy()
        new = pygame.transform.rotate(sprite, self.rotation(entity.rotation))
        sprite.fill((0, 0, 0, 0))
        sprite.blit(
            new, [(o - n) // 2 for n, o in zip(new.get_size(), sprite.get_size())]
        )
        return sprite


class Bounce(Animation):
    def __init__(self, thing, pos, duration=0.2):
        super().__init__(thing, duration)
        self.old_pos = pos

    def position(self, new_pos):
        p = abs(0.5 - self.progress) + 0.5
        return lerp(hexes.VecF(self.old_pos), hexes.VecF(new_pos), p)

    def start(self):
        HIT_SOUND.play()


class Death(Animation):
    def __init__(self, thing, duration=0.2):
        super().__init__(thing, duration)

    def opacity(self):
        return (1 - self.progress) * 255

    def sprite(self, sprite):
        sprite = sprite.copy()
        sprite.set_alpha(self.opacity())
        return sprite


class Spawn(Death):
    def __init__(self, *args, siren=False, **kwargs):
        self.siren = siren
        super().__init__(*args, **kwargs)

    def opacity(self):
        return self.progress * 255

    def start(self):
        if self.siren:
            SIREN_SOUND.play()


class Beam(Animation):
    def __init__(self, *points, duration=0.2):
        super().__init__(None, duration)
        self.points = points
        self.draw_points = [
            (p[0] + 4, p[1] + 4) for p in [x.cartesian(3) for x in self.points]
        ]
        self.surface = pygame.Surface(
            [abs(self.draw_points[1][i] - self.draw_points[0][i]) + 2 for i in range(2)]
        )
        self.surface.set_colorkey((0, 0, 0))
        pygame.draw.line(
            self.surface,
            colours.BEAM_COL,
            (0, 0),
            [i - 2 for i in self.surface.get_size()],
            2,
        )
        if (self.draw_points[1][0] - self.draw_points[0][0]) * (
            self.draw_points[1][1] - self.draw_points[0][1]
        ) < 0:
            self.surface = pygame.transform.flip(self.surface, True, False)

    def draw(self, surface, origin_pos):
        self.surface.set_alpha((1 - self.progress) * 255)
        surface.blit(
            self.surface,
            [
                origin_pos[i] + min(self.draw_points[1][i], self.draw_points[0][i]) - 1
                for i in range(2)
            ],
        )

    def start(self):
        BEAM_SOUND.play()


class CardMove(Animation):
    def __init__(self, thing, old_pos, duration=0.2):
        super().__init__(thing, duration)
        self.old_pos = old_pos

    def position(self, new_pos):
        return [self.lerp(self.old_pos[i], new_pos[i]) for i in range(2)]


class QueueSlide(Animation):
    def __init__(self, thing, pixels, duration=0.2):
        self.pixels = pixels
        super().__init__(thing, duration)

    def position(self):
        return (1 - self.progress) * self.pixels


class CardAppear(Animation):
    def __init__(self, thing, old_pos, duration=0.2):
        super().__init__(thing, duration)
        self.old_pos = old_pos

    def position(self, new_pos):
        return [self.lerp(self.old_pos[i], new_pos[i]) for i in range(2)]

    def resized_sprite(self, sprite, back_sprite, delay=0):
        progress = (self.progress - delay) / (1 - delay)
        if progress <= 0:
            return back_sprite.copy()
        sprite = sprite if progress >= 0.5 else back_sprite
        new = pygame.Surface(sprite.get_size(), pygame.SRCALPHA)
        shrunk = pygame.transform.scale(
            sprite,
            (
                round(abs(math.cos(progress * math.pi)) * sprite.get_width()),
                sprite.get_height(),
            ),
        )
        new.blit(shrunk, (round((sprite.get_width() - shrunk.get_width()) / 2), 0))
        return new


class CardUse(Animation):
    def __init__(self, thing, duration=0.2):
        super().__init__(thing, duration)

    def position(self, old_pos, new_pos):
        return [self.lerp(old_pos[i], new_pos[i]) for i in range(2)]

    def opacity(self):
        return (1 - self.progress) * 255


class CardAdd(CardAppear):
    def resized_sprite(self, sprite, back_sprite):
        sprite = super().resized_sprite(back_sprite, sprite, delay=0.7)
        sprite.set_alpha(min(self.progress * 1000, 255))
        return sprite


class TileFade(Animation):
    def __init__(self, position, old_type, duration=0.2):
        super().__init__(position, duration=0.2)
        self.old_type = old_type

    def sprite(self, new_type, sprites):
        old_sprite = sprites[self.old_type]
        new_sprite = sprites[new_type]
        old_im = pygame.Surface(old_sprite.get_size(), pygame.SRCALPHA)
        new_im = pygame.Surface(new_sprite.get_size(), pygame.SRCALPHA)
        old_im.fill([255 * (1 - self.progress) for _ in range(3)])
        new_im.fill([255 * self.progress for _ in range(3)])
        old_im.blit(old_sprite, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
        new_im.blit(new_sprite, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
        new_im.blit(old_im, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)
        return new_im


class FadeBlack(Animation):
    def __init__(self, old_image, duration=0.5):
        super().__init__(None, duration)
        self.old_image = old_image.copy()

    def image(self, new_image):
        if self.progress == 0:
            return self.old_image
        if self.progress == 1:
            return new_image
        if self.progress < 0.5:
            progress = 1 - 2 * self.progress
            image = self.old_image
        else:
            progress = 2 * self.progress - 1
            image = new_image
        im = pygame.Surface(image.get_size(), pygame.SRCALPHA)
        im.fill([255 * progress for _ in range(3)])
        im.blit(image, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
        return im


class MessageSlide(Animation):
    def __init__(self, message, font, direction="up", duration=4):
        super().__init__(None, duration)
        self.message = message
        self.text = font.render(self.message, False, colours.TEXT_COL)
        self.direction = direction

    def image(self, surface):
        if not self.progress:
            return surface
        progress = min(self.progress * 3, 1)
        new_im = pygame.Surface(surface.get_size())
        new_im.fill(colours.BACK_COL)
        scale = 0.9 * new_im.get_width() / self.text.get_width()
        text = pygame.transform.scale(
            self.text, [round(x * scale) for x in self.text.get_size()]
        )
        direction = {"up": 1, "down": -1}[self.direction]
        draw_y = direction * new_im.get_height() * progress

        new_im.blit(surface, (0, draw_y))
        new_im.blit(
            text,
            (
                (new_im.get_width() - text.get_width()) / 2,
                round(
                    draw_y
                    - direction * new_im.get_height()
                    + (new_im.get_height() - text.get_height()) / 2
                ),
            ),
        )
        return new_im

    def start(self):
        if self.direction == "down":
            DEATH_SOUND.play()


class AnimationList:
    def __init__(self, animations=None):
        self.animations = [] if animations is None else animations
        self.shielding = None

    def translate(self, *args, **kwargs):
        self.animations.append(Translation(*args, **kwargs))

    def jump(self, *args, **kwargs):
        self.animations.append(Jump(*args, **kwargs))

    def bounce(self, *args, **kwargs):
        self.animations.append(Bounce(*args, **kwargs))

    def rotate(self, *args, **kwargs):
        self.animations.append(Rotation(*args, **kwargs))

    def kill(self, *args, **kwargs):
        self.animations.append(Death(*args, **kwargs))

    def spawn(self, *args, **kwargs):
        self.animations.append(Spawn(*args, **kwargs))

    def create_beam(self, *args, **kwargs):
        self.animations.append(Beam(*args, **kwargs))

    def play_card(self, *args, **kwargs):
        self.animations.append(CardMove(*args, **kwargs))

    def slide_queue(self, *args, **kwargs):
        self.animations.append(QueueSlide(*args, **kwargs))

    def draw_card(self, *args, **kwargs):
        self.animations.append(CardAppear(*args, **kwargs))

    def use_card(self, *args, **kwargs):
        self.animations.append(CardUse(*args, **kwargs))

    def damage(self, damage_type):
        self.animations.append(CardAdd(damage_type, (0, 0), duration=1))

    def fade_tile(self, *args, **kwargs):
        self.animations.append(TileFade(*args, **kwargs))

    def fade_black(self, *args, **kwargs):
        self.animations.append(FadeBlack(*args, **kwargs))

    def show_message(self, *args, **kwargs):
        self.animations.append(MessageSlide(*args, **kwargs))

    @property
    def current(self):
        return self.animations[0]

    def remove_damage(self):
        self.animations = [a for a in self.animations if not isinstance(a, CardAdd)]

    def current_position(self, entity):
        if self:
            if self.current.thing is entity and isinstance(
                self.current, (Translation, Bounce)
            ):
                return self.current.position(entity.position)
            for a in self.animations[1:]:
                if entity is a.thing and isinstance(a, Translation):
                    return a.old_pos
        return entity.position

    def current_sprite(self, entity):
        sprite = entity.sprite
        if self:
            for a in self.animations[1:]:
                if a.thing is entity and isinstance(a, Rotation):
                    sprite = entity.sprites[a.old_rot]
                elif a.thing is entity and isinstance(a, Spawn):
                    sprite = a.sprite(sprite)
            if self.current.thing is entity:
                if isinstance(self.current, (Death, Jump)):
                    sprite = self.current.sprite(sprite)
                elif isinstance(self.current, Rotation):
                    sprite = self.current.sprite(sprite, entity)
            if entity is self.shielding:
                shield = pygame.Surface(sprite.get_size(), pygame.SRCALPHA)
                shield.fill((150, 150, 255))
                shield.blit(sprite, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
                sprite = shield
        return sprite

    def card_not_arrived(self, card):
        return any(isinstance(a, CardAppear) and a.thing == card for a in self)

    def card_position(self, card, new_pos, sprite, back_sprite):
        for i, a in enumerate(self.animations):
            if isinstance(a, CardAppear) and a.thing == card:
                if i > 0:
                    return None, None
                return a.resized_sprite(sprite, back_sprite), a.position(new_pos)
        return sprite, new_pos

    def card_add_position(self, new_pos, sprites):
        for i, a in enumerate(self.animations):
            if isinstance(a, CardAdd):
                if i > 0:
                    return None, None
                return (
                    a.resized_sprite(sprites[a.thing], sprites["back"]),
                    a.position(new_pos),
                )
        return None, None

    def played_pos(self, new_pos):
        for a in self:
            if isinstance(a, CardMove) and a.thing == "played":
                return a.position(new_pos)
        return new_pos

    def queue_shift(self):
        for a in self:
            if isinstance(a, QueueSlide):
                return a.position()
        return 0

    def acting_card(self, old_pos, new_pos, sprites):
        for a in self:
            if isinstance(a, CardUse):
                sprite = sprites[a.thing].copy()
                sprite.set_alpha(a.opacity())
                return sprite, a.position(old_pos, new_pos)
        return None, None

    def tile(self, pos, new_type, tile_set):
        for i, a in enumerate(self):
            if isinstance(a, TileFade) and a.thing == pos:
                if i:
                    return tile_set[a.old_type]
                return a.sprite(new_type, tile_set)
        return tile_set[new_type]

    def draw(self, surface, origin):
        for a in self:
            if isinstance(a, Death):
                a.thing.draw(surface, origin)
        if self.animations:
            if isinstance(self.current, Beam):
                self.current.draw(surface, origin)

    def tick(self, time):
        while self and time > 0:
            self.current.progress += time / self.current.duration
            if self.current.progress >= 1:
                time = (self.current.progress - 1) * self.current.duration
                self.animations.pop(0)
                if self:
                    self.current.start()
            else:
                time = 0

    def show_fade(self, surface):
        for a in self:
            if isinstance(a, (FadeBlack, MessageSlide)):
                surface.blit(a.image(surface), (0, 0))

    def __iter__(self):
        return iter(self.animations)

    def __bool__(self):
        return bool(self.animations)

    def clear(self):
        self.animations = []

    def __repr__(self):
        return f"{self.__class__.__qualname__}({self.animations})"
