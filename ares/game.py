import math

import pygame

from . import animations, colours, deck, menus, world


def scale_up(image, factor):
    return pygame.transform.scale(image, [int(x * factor) for x in image.get_size()])


CARD_HOVER_LABEL_DELAY = 0.8

FPS = 60

COLLECT_SOUND = pygame.mixer.Sound("ares/data/sounds/collect.wav")


class Game:
    def __init__(self, S, tile_set, entity_sprites, card_images, font):
        self.S = S
        self.tile_set = tile_set
        self.entity_sprites = entity_sprites
        self.card_images = card_images
        self.FONT = font
        self.clock = pygame.time.Clock()
        self.cops_im = self.FONT.render("Cops:", False, colours.COP_DOT_COL)
        self.cops_here_im = self.FONT.render("Cops!", False, colours.COP_DOT_COL)
        self.choose_im = self.FONT.render(
            "Choose a part", False, colours.CHOOSE_TEXT_COL
        )
        self.setup()

    def setup(self):
        self.hovered_card = None
        self.hovered_tile = None
        self.scale = None
        self.animations = animations.AnimationList()
        self.deck = deck.Deck(self.card_images, self.animations)
        self.result = None
        self.level = 1
        self.setup_level()

    def setup_level(self, player_rotation=5):
        self.got_chest = False
        self.world = world.World(
            self.tile_set,
            self.entity_sprites,
            3 + self.level // 2,
            self.level,
            player_rotation,
            self.animations,
        )
        self.level_im = self.FONT.render(
            f"Level {self.level}/9" if self.level <= 9 else f"Level {self.level}",
            False,
            colours.TEXT_COL,
        )
        if self.level == 9:
            animations.SIREN_SOUND.play()
        self.turns_to_cops = 21 + self.level * 9

    def progress_level(self):
        remove_count = min((self.level // 3) + 1, len(self.deck) - 15)
        if remove_count > 0:
            choose_remove_im = self.FONT.render(
                "Choose a part to discard"
                if remove_count == 1
                else f"Choose {remove_count} parts to discard",
                False,
                colours.CHOOSE_TEXT_COL,
            )
            self.deck.redeal_without(
                self.card_select_grid(
                    self.deck.all_cards, choose_remove_im, 1, remove_count
                )
            )
        self.animations.fade_black(self.S)
        self.level += 1
        self.setup_level(self.world.player.rotation)

    def play(self):
        while True:
            result = self.update()
            if result == "restart":
                self.setup()
                continue
            elif result == "give up":
                break
            self.draw()
            pygame.display.update()
            self.animations.tick(1 / FPS)
            if self.result is None:
                if result:
                    self.result = result
            elif not self.animations:
                if self.result == "hole":
                    break
                elif self.result == "exit":
                    self.progress_level()
                    self.result = None
            self.clock.tick(FPS)

    def handle_event(self, event):
        if event.type == pygame.QUIT:
            raise SystemExit
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.hovered_card is None:
                return
            return self.make_move(self.hovered_card)
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                return self.pause_menu()

    def pause_menu(self):
        self.S.fill(colours.DARKEN_COL, special_flags=pygame.BLEND_MULT)
        back = self.S.copy()
        menu = menus.Menu("Paused", ["Continue", "Restart", "Give Up"])
        while True:
            result = menu.update()
            if result == "Continue":
                break
            elif result == "Restart":
                return "restart"
            elif result == "Give Up":
                return "give up"
            self.S.blit(pygame.transform.scale(back, self.S.get_size()), (0, 0))
            menu.draw(self.S)
            pygame.display.update()
            self.clock.tick(60)

    def make_move(self, card):
        self.animations.clear()
        card = self.deck.play(card)
        shielding = False
        if card is not None:
            if (done := self.world.move_player(card)) :
                if done == "chest":
                    self.got_chest = True
                elif done == "hole":
                    self.animations.show_message(
                        "Ares destroyed.", self.FONT, direction="down"
                    )
                    return done
                elif done == "exit":
                    if self.level == 8:
                        self.animations.show_message(
                            "They were waiting for you down here!",
                            self.FONT,
                            direction="up",
                        )
                    if self.level == 9:
                        self.animations.show_message(
                            "You escaped! See how far you can get...",
                            self.FONT,
                            direction="up",
                        )
                    return done
                elif done == "shielded":
                    shielding = True
        self.animations.shielding = self.world.player if shielding else None
        for damage in self.world.move_enemies():
            if not shielding:
                deck.insert_random(self.deck.cards, damage)
        if shielding:
            self.animations.remove_damage()
        self.turns_to_cops -= 1
        if self.turns_to_cops <= 0:
            self.world.spawn_cop(self.turns_to_cops == 0)

    def map_width(self):
        return round(self.S.get_width() * 0.4)

    def card_select_grid(self, choices, message_im, scale=1, count=1):
        COLLECT_SOUND.play()
        target_scale = self.scale * scale
        self.S.fill(colours.DARKEN_COL, special_flags=pygame.BLEND_MULT)
        original_message_im = message_im
        back = self.S.copy()
        chosen = []
        hovering = None
        while True:
            mp = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    raise SystemExit
                elif event.type == pygame.MOUSEBUTTONUP:
                    if hovering is not None:
                        chosen.append(hovering)
                        if len(chosen) == count:
                            return [choices[i] for i in chosen]

            message_im = scale_up(original_message_im, self.scale)
            remaining_height = self.S.get_height() - message_im.get_height() - 5 * scale
            grid_width = math.ceil(
                math.sqrt(len(choices) * self.S.get_width() / remaining_height)
            )
            grid_height = math.ceil(len(choices) / grid_width)
            card_size = next(iter(self.deck.card_images.values())).get_size()
            scale = min(
                target_scale,
                self.S.get_width() / (grid_width * (card_size[0] + 4)),
                remaining_height / (grid_height * (card_size[1] + 4)),
            )

            self.S.blit(pygame.transform.scale(back, self.S.get_size()), (0, 0))
            self.S.blit(
                message_im,
                ((self.S.get_width() - message_im.get_width()) // 2, scale * 3),
            )
            label = None
            for i, choice in enumerate(choices):
                if i in chosen:
                    continue
                image = self.deck.card_images[choice]
                full_size = (
                    grid_width * image.get_width() + (grid_width - 1) * 4,
                    grid_height * image.get_height() + (grid_height - 1) * 4,
                )
                grid_pos = (
                    i % grid_width * (4 + image.get_width()),
                    i // grid_width * (4 + image.get_height()),
                )
                draw_pos = [
                    (x / scale - full_size[i]) / 2 + grid_pos[i]
                    for i, x in enumerate((self.S.get_width(), remaining_height))
                ]
                draw_pos = [int(scale * x) for x in draw_pos]
                draw_pos[1] += message_im.get_height() + 4 * scale
                self.S.blit(scale_up(image, scale), draw_pos)
                relative_mouse_pos = [
                    int((mp[x] - draw_pos[x]) / scale) for x in range(2)
                ]
                if (
                    all(
                        0 <= x < s for x, s in zip(relative_mouse_pos, image.get_size())
                    )
                    and image.get_at(relative_mouse_pos)[3]
                ):
                    self.S.blit(
                        scale_up(self.deck.card_images["hover_card"], scale), draw_pos
                    )
                    hovering = i
                    self.hover_time += 1 / FPS
                    if self.hover_time > CARD_HOVER_LABEL_DELAY:
                        label = deck.label(
                            choice, self.FONT, colours.TEXT_COL, colours.CHOOSE_TEXT_COL
                        )
                        draw_pos = [
                            draw_pos[0]
                            - (
                                (
                                    self.scale * label.get_width()
                                    - scale * image.get_width()
                                )
                                // 2
                            ),
                            draw_pos[1] - 10 * self.scale,
                        ]
                        label = scale_up(label, self.scale)
                        draw_pos = [
                            min(
                                max(draw_pos[i], 0),
                                self.S.get_size()[i] - label.get_size()[i],
                            )
                            for i in range(2)
                        ]

                        label = (label, draw_pos)
                    else:
                        label = "waiting"
            if label:
                if label != "waiting":
                    self.S.blit(*label)
            else:
                self.hover_time = 0
            pygame.display.update()

    def choose_new_card(self):
        self.got_chest = False
        choices = deck.new_card_options(self.level)
        card = self.card_select_grid(choices, self.choose_im, 2)[0]
        self.animations.damage(card)
        deck.insert_random(self.deck.cards, card)

    def update(self):
        if self.result is None and not self.got_chest:
            if self.scale is not None:
                mp = pygame.mouse.get_pos()
                self.hovered_card = self.deck.hovering(
                    [
                        int((mp[i] - self.hand_draw_pos[i]) / self.scale)
                        for i in range(2)
                    ]
                )
                self.hovered_tile = self.world.tile_from_xy(
                    [
                        int((mp[i] - self.world_draw_pos[i]) / self.scale)
                        for i in range(2)
                    ]
                )
        else:
            self.hovered_card = None
            self.deck.hover = None

        if self.got_chest and not self.animations:
            self.choose_new_card()

        for event in pygame.event.get():
            if done := self.handle_event(event):
                return done

    def draw(self):
        self.S.fill(colours.BACK_COL)
        world_image = self.world.draw()
        hand_image = self.deck.draw()
        hand_image_width = (
            hand_image.get_width()
            * len(self.deck.hand)
            / (len(self.deck.hand) + self.deck.left_zone)
        )
        self.scale = min(
            self.S.get_height() / (world_image.get_height() + 2),
            self.S.get_width() / (world_image.get_width() + hand_image_width + 3),
        )
        self.world_draw_pos = (
            self.scale,
            (self.S.get_height() - self.scale * world_image.get_height()) // 2,
        )
        self.S.blit(scale_up(world_image, self.scale), self.world_draw_pos)

        self.hand_draw_pos = [
            (s - self.scale * (h + 1))
            for s, h in zip(self.S.get_size(), hand_image.get_size())
        ]
        self.S.blit(scale_up(hand_image, self.scale), self.hand_draw_pos)

        self.S.blit(
            scale_up(self.level_im, self.scale),
            (
                self.S.get_width() - (self.level_im.get_width() + 1) * self.scale,
                self.scale,
            ),
        )
        cops_im = self.cops_im if self.turns_to_cops > 0 else self.cops_here_im
        self.S.blit(
            scale_up(cops_im, self.scale),
            (
                self.S.get_width() - (self.cops_here_im.get_width() + 1) * self.scale,
                self.scale * 12,
            ),
        )
        for i in range(self.turns_to_cops):
            pygame.draw.rect(
                self.S,
                colours.COP_DOT_COL,
                (
                    self.S.get_width() - self.scale * ((i // 3) * 2 + 2),
                    self.scale * (25 + (i % 3) * 2),
                    self.scale,
                    self.scale,
                ),
            )

        if self.hovered_card is not None:
            self.hover_time += 1 / FPS
            if self.hover_time > CARD_HOVER_LABEL_DELAY:
                label = scale_up(
                    deck.label(
                        self.deck.hand[self.hovered_card], self.FONT, colours.TEXT_COL
                    ),
                    self.scale,
                )
                self.S.blit(
                    label,
                    (
                        (world_image.get_width() + 2) * self.scale,
                        self.S.get_height()
                        - (hand_image.get_height() + 3) * self.scale
                        - label.get_height(),
                    ),
                )
        elif self.hovered_tile:
            label = scale_up(
                self.FONT.render(self.hovered_tile, False, colours.TEXT_COL), self.scale
            )
            self.S.blit(
                label,
                (
                    (world_image.get_width() + 2) * self.scale,
                    self.S.get_height()
                    - (hand_image.get_height() + 3) * self.scale
                    - label.get_height(),
                ),
            )
        else:
            self.hover_time = 0

        self.animations.show_fade(self.S)
