from pathlib import Path

import pygame

from . import colours, game, hexes, tutorial


def load_image(directory, name):
    return pygame.image.load(
        Path("ares/data") / directory / f"{name}.png"
    ).convert_alpha()


def get_tiles():
    tiles = {
        t: load_image("tiles", t)
        for t in ("dirt", "rock", "hole", "exit", "entrance", "chest")
    }
    for on_top in ("rock", "hole", "exit", "entrance", "chest"):
        temp = tiles[on_top]
        tiles[on_top] = tiles["dirt"].copy()
        tiles[on_top].blit(temp, (0, 0))
    return tiles


def get_rotated_sprites(sprite_name):
    images = {t: load_image("entities", f"{sprite_name}_{t}") for t in ("ea", "ne")}
    images["we"] = pygame.transform.flip(images["ea"], True, False)
    images["nw"] = pygame.transform.flip(images["ne"], True, False)
    images["se"] = pygame.transform.flip(images["ne"], False, True)
    images["sw"] = pygame.transform.flip(images["ne"], True, True)
    return [images[i] for i in hexes.DIRECTION_NAMES]


def get_card_images():
    images = {
        c: load_image("cards", c)
        for c in (
            "hover_card",
            "back",
            "blank",
            "random",
            "f1",
            "f2",
            "f3",
            "f4",
            "r1",
            "r2",
            "r3",
            "j2",
            "j3",
            "laser",
            "laser_f1",
            "twolaser",
            "threelaser",
            "triplelaser",
            "hexalaser",
            "shield",
            "f1_shield",
            "laser_shield",
        )
    }
    images["b1"] = pygame.transform.flip(images["f1"], False, True)
    images["b2"] = pygame.transform.flip(images["f2"], False, True)
    images["l1"] = pygame.transform.flip(images["r1"], True, False)
    images["l2"] = pygame.transform.flip(images["r2"], True, False)
    return images


class Menu:
    gap = 5

    def __init__(self, title, items, highlight_title=False):
        self.title = title
        self.items = items
        self.title_image = FONT.render(
            self.title,
            False,
            (200, 60, 60) if highlight_title else colours.CHOOSE_TEXT_COL,
        )
        self.images = [
            FONT.render(i, False, colours.CHOOSE_TEXT_COL) for i in self.items
        ]
        self.hover_images = [
            FONT.render(i, False, colours.TEXT_HOVER_COL) for i in self.items
        ]
        self.max_width = max(i.get_width() for i in self.images)
        self.hovering = None
        self.scale = 1
        self.draw_size = (1, 1)
        self.offset = 100

    def update(self):
        if self.offset > 0:
            self.offset *= 0.75
        mp = pygame.mouse.get_pos()
        self.hovering = None
        for i, (item, image) in enumerate(zip(self.items, self.images)):
            top_left = self.draw_pos(i)
            if all(
                top_left[i] <= mp[i] <= top_left[i] + image.get_size()[i] * self.scale
                for i in range(2)
            ):
                self.hovering = item
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise SystemExit
            elif event.type == pygame.MOUSEBUTTONUP:
                return self.hovering

    def draw_pos(self, index):
        return (
            (self.draw_size[0] - self.images[index].get_width() * self.scale) // 2,
            (2 + index) * self.scale * (7 + self.gap) + self.offset * self.scale,
        )

    def draw(self, S):
        self.draw_size = S.get_size()
        self.scale = min(
            self.draw_size[1] / (2 * len(self.items) * (7 + self.gap)),
            self.draw_size[0] / (self.max_width * 2),
        )
        for i, item in enumerate(self.items):
            S.blit(
                game.scale_up(
                    (self.hover_images if item == self.hovering else self.images)[i],
                    self.scale,
                ),
                self.draw_pos(i),
            )
        title = game.scale_up(self.title_image, self.scale * 2)
        S.blit(
            title,
            (
                (S.get_width() - title.get_width()) / 2,
                2 * self.scale + self.offset * self.scale,
            ),
        )


def draw_centred(S, image):
    scale = max(S.get_width() / image.get_width(), S.get_height() / image.get_height())
    new_size = [round(i * scale) for i in image.get_size()]
    S.blit(
        pygame.transform.scale(image, new_size),
        [(s - n) // 2 for n, s in zip(new_size, S.get_size())],
    )


INTRO_COUNT = 3


def main():
    pygame.init()
    S = pygame.display.set_mode((1280, 720), pygame.RESIZABLE)
    pygame.display.set_caption("Ares")
    image = pygame.image.load("ares/data/intro/0.png")
    draw_centred(S, image)
    pygame.display.update()
    player_sprites = get_rotated_sprites("player")
    icon = pygame.Surface((64, 64), pygame.SRCALPHA)
    icon.blit(pygame.transform.scale(player_sprites[0], (84, 96)), (-10, -16))
    pygame.display.set_icon(icon)
    global FONT
    FONT = pygame.font.Font("ares/data/font/generic-pixel-font-5x7-neue.ttf", 15)
    tile_set = get_tiles()
    for t in tile_set:
        tile_set[t] = tile_set[t].convert_alpha()
    card_images = get_card_images()
    entity_sprites = {
        "player": player_sprites,
        "critter": load_image("entities", "critter"),
        "ghost": load_image("entities", "ghost"),
        "bat": load_image("entities", "bat"),
        "cop": get_rotated_sprites("cop"),
        "bull": get_rotated_sprites("bull"),
    }
    menu = Menu("Ares", ["Play", "Tutorial", "Quit"], highlight_title=True)
    ext = False
    intro = 0
    clock = pygame.time.Clock()
    while not ext:
        for event in pygame.event.get():
            if event.type in {pygame.KEYUP, pygame.MOUSEBUTTONUP}:
                intro += 1
                if intro == INTRO_COUNT:
                    ext = True
                    break
                else:
                    image = pygame.image.load(f"ares/data/intro/{intro}.png")
            elif event.type == pygame.QUIT:
                raise SystemExit
        draw_centred(S, image)
        pygame.display.update()
        clock.tick(60)
    while True:
        result = menu.update()
        if result == "Play":
            the_game = game.Game(S, tile_set, entity_sprites, card_images, FONT)
            the_game.play()
        elif result == "Tutorial":
            the_tutorial = tutorial.Tutorial(
                S, tile_set, entity_sprites, card_images, FONT
            )
            the_tutorial.play()
        elif result == "Quit":
            break
        draw_centred(S, image)
        menu.draw(S)
        pygame.display.update()
        clock.tick(60)
