import math
import random

import pygame

from . import colours, enemies, hexes, player


class DrawableGrid(hexes.Grid):
    def __init__(self, radius, tile_set):
        self.tile_set = tile_set
        super().__init__(radius)

    def draw(self, surface, origin_pos, animations):
        for p in self.positions:
            tile = animations.tile(p, self[p], self.tile_set)
            draw_pos = p.cartesian(3)
            draw_pos = (origin_pos[0] + draw_pos[0], origin_pos[1] + draw_pos[1])
            surface.blit(tile, draw_pos)

    def position_from_xy(self, xy, origin_pos):
        mini = 4
        result = None
        for p in self.positions:
            draw_pos = p.cartesian(3)
            draw_pos = (origin_pos[0] + draw_pos[0], origin_pos[1] + draw_pos[1])
            centre = (draw_pos[0] + 3, draw_pos[1] + 3.5)
            d = math.dist(centre, xy)
            if d < mini:
                result = p
                mini = d
        return result


class World:
    def __init__(
        self, tile_set, entity_sprites, size, level, player_rotation, animations
    ):
        self.tile_set = tile_set
        self.entity_sprites = entity_sprites
        self.size = size
        self.level = level
        self.animations = animations
        self.tiles = DrawableGrid(self.size, tile_set)
        self.entities = []
        self.entrance_position = hexes.Vec(
            0, -round(self.size * 0.7), round(self.size * 0.7)
        ) * (-1) ** (self.level)
        self.player = player.Player(
            entity_sprites["player"],
            self.entrance_position,
            player_rotation,
            self.animations,
        )
        hole_near_stairs_tolerance = 2 if self.level == 1 else 1
        for p in self.tiles.positions:
            if p == self.entrance_position:
                tile = "entrance"
            elif p == -self.entrance_position:
                tile = "exit"

            elif random.random() < (0.4 - 0.35 * 0.9 ** self.level):
                tile = "hole"
            elif random.random() < (0.3 - 0.25 * 0.9 ** self.level):
                tile = "rock"
            else:
                tile = "dirt"

            if tile == "hole" and (
                abs(self.entrance_position - p) <= hole_near_stairs_tolerance
                or abs(-self.entrance_position - p) <= hole_near_stairs_tolerance
            ):
                tile = "dirt"
            self.tiles[p] = tile
            if (
                tile == "dirt"
                and abs(p - self.entrance_position) > max(3 - self.level // 2, 2)
                and random.random() < (0.5 * (1 - 0.95 ** (max(self.level, 2) + 1)))
            ):
                if random.random() < (0.4 * (1 - 0.9 ** (self.level - 3))):
                    self.entities.append(
                        enemies.Ghost(entity_sprites["ghost"], p, self.animations)
                    )
                elif random.random() < (0.2 * (1 - 0.9 ** (self.level - 5))):
                    self.entities.append(
                        enemies.Bat(entity_sprites["bat"], p, self.animations)
                    )

                elif random.random() < (0.5 * (1 - 0.9 ** (self.level - 2))):
                    self.entities.append(
                        enemies.Bull(
                            entity_sprites["bull"],
                            p,
                            random.randrange(6),
                            self.animations,
                        )
                    )
                else:
                    self.entities.append(
                        enemies.Critter(entity_sprites["critter"], p, self.animations)
                    )
        for _ in range(self.level // 3 + 1):
            while True:
                chest_pos = hexes.random(self.size)
                if all(
                    (
                        abs(chest_pos - self.entrance_position) > 2,
                        abs(chest_pos - -self.entrance_position) > 2,
                        self.entity_on_cell(chest_pos) is None,
                    )
                ):
                    break
            self.tiles[chest_pos] = "chest"
        if self.level == 9:
            for _ in range(25):
                cop_pos = -self.entrance_position + hexes.random(3)
                if (
                    abs(cop_pos) <= self.size
                    and self.entity_on_cell(cop_pos) is None
                    and self.tiles[cop_pos] == "dirt"
                ):
                    self.entities.append(
                        enemies.Cop(
                            entity_sprites["cop"],
                            cop_pos,
                            random.randrange(6),
                            self.animations,
                        )
                    )
        self.entities.append(self.player)
        self.surface = pygame.Surface(
            ((2 * self.size + 1) * 6 + 1, (2 * self.size + 1) * 6 + 2)
        )

    def can_enter(self, cell, ignore_rock=False, ignore_entities=False):
        return (
            abs(cell) <= self.size
            and (self.tiles[cell] != "rock" or ignore_rock)
            and (self.entity_on_cell(cell) is None or ignore_entities)
        )

    def kill_in_hole(self, entity):
        if self.tiles[entity.position] == "hole":
            self.kill(entity)

    def kill(self, entity):
        self.animations.kill(entity)
        self.entities.remove(entity)

    def entity_on_cell(self, cell):
        try:
            return next(e for e in self.entities if e.position == cell)
        except StopIteration:
            return None

    def spawn_cop(self, siren=False):
        if self.can_enter(self.entrance_position):
            cop = enemies.Cop(
                self.entity_sprites["cop"],
                self.entrance_position,
                random.randrange(6),
                self.animations,
            )
            self.animations.spawn(cop, siren=siren)
            self.entities.append(cop)

    def draw(self):
        self.surface.fill(colours.BACK_COL)
        self.tiles.draw(self.surface, (self.size * 6, self.size * 6), self.animations)
        for entity in self.entities:
            entity.draw(self.surface, (self.size * 6, self.size * 6))
        self.animations.draw(self.surface, (self.size * 6, self.size * 6))
        return self.surface

    def move_player(self, move):
        return self.player.move(move, self)

    def move_enemies(self):
        random.shuffle(self.entities)
        damage = []
        for entity in self.entities:
            if isinstance(entity, player.Player):
                continue
            new_damage = entity.move(self)
            if new_damage is None:
                continue
            damage += new_damage
        return damage

    def tile_from_xy(self, xy):
        position = self.tiles.position_from_xy(xy, (self.size * 6, self.size * 6))
        if position is None:
            return None
        entity = self.entity_on_cell(position)
        if entity is not None:
            return entity.name
        return self.tiles[position].title()
