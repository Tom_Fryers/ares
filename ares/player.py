import random

from . import enemies, hexes


class Player(enemies.RotatableEntity):
    name = "Ares"

    def move(self, move, world):
        for move in move.split("_"):
            if move[-1] in "0123456789":
                move_type = move[: -len(move.rstrip("0123456789"))]
                amount = int(move[len(move_type) :])
            else:
                move_type = move
                amount = None
            if (result := self.do_move(move_type, amount, world)) is not None:
                return result

    def fire_multilaser(self, world, positions):
        for i in positions:
            entity_hit = self.fire_beam(
                world,
                position=(self.position + hexes.DIRECTIONS[(self.rotation + i) % 6]),
            )
            if entity_hit:
                world.kill(entity_hit)

    def fire_multidirectional_laser(self, world, angles):
        for i in angles:
            entity_hit = self.fire_beam(world, angle=(self.rotation + i) % 6)
            if entity_hit:
                world.kill(entity_hit)

    def do_move(self, move_type, amount, world):
        if move_type == "random":
            move_type, amounts = random.choice(
                (
                    ("f", (1, 2, 3)),
                    ("b", (1,)),
                    ("l", (1, 2)),
                    ("r", (1, 2)),
                    ("blank", (None,)),
                )
            )
            amount = random.choice(amounts)

        if move_type == "blank":
            pass
        elif move_type == "shield":
            return "shielded"
        elif move_type == "laser":
            entity_hit = self.fire_beam(world)
            if entity_hit:
                world.kill(entity_hit)
        elif move_type == "triplelaser":
            self.fire_multidirectional_laser(world, (-1, 0, 1))
        elif move_type == "hexalaser":
            self.fire_multidirectional_laser(world, range(6))
        elif move_type == "twolaser":
            self.fire_multilaser(world, (-1, 1))
        elif move_type == "threelaser":
            self.fire_multilaser(world, (-1, 0, 1))
        elif move_type in {"l", "r"}:
            self.animations.rotate(self, self.rotation)
            self.rotation += amount * {"l": 1, "r": -1}[move_type]
        elif move_type in {"f", "b", "j"}:
            result = None
            bounced_off = None
            will_crush = None
            will_kill = None
            old_pos = self.position
            for i in range(amount if move_type in "fb" else 1):
                if move_type == "f":
                    new_pos = self.in_front
                elif move_type == "b":
                    new_pos = self.behind
                elif move_type == "j":
                    new_pos = self.position
                    for _ in range(amount):
                        if abs(new_pos + hexes.DIRECTIONS[self.rotation]) > world.size:
                            bounced_off = new_pos + hexes.DIRECTIONS[self.rotation]
                            break
                        new_pos += hexes.DIRECTIONS[self.rotation]
                if world.can_enter(new_pos):
                    self.position = new_pos
                    if world.tiles[new_pos] in {"hole", "exit"}:
                        result = world.tiles[new_pos]
                        break
                    elif world.tiles[new_pos] == "chest":
                        self.animations.fade_tile(new_pos, "chest")
                        world.tiles[new_pos] = "dirt"
                        result = "chest"
                else:
                    if move_type == "j":
                        self.position = new_pos
                        if world.tiles[new_pos] == "rock":
                            will_crush = (new_pos, "rock")
                            world.tiles[new_pos] = "dirt"
                        if (
                            entity := world.entity_on_cell(new_pos)
                        ) and entity is not self:
                            will_kill = entity
                    else:
                        bounced_off = new_pos
                        break
            distance = abs(self.position - old_pos)
            if distance > 0:
                if move_type == "j":
                    self.animations.jump(self, old_pos, duration=0.2 * distance)
                else:
                    self.animations.translate(self, old_pos, duration=0.2 * distance)
            if bounced_off is not None:
                self.animations.bounce(self, bounced_off, duration=0.2)
            if will_crush:
                self.animations.fade_tile(*will_crush)
            if will_kill:
                world.kill(will_kill)
            if result:
                return result
        self.rotation %= 6
