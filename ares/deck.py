import random

import pygame

from . import colours

CARDS = {
    "random": "Random",
    "blank": "Stay Still",
    "f1": "Forward 1",
    "f2": "Forward 2",
    "f3": "Forward 3",
    "f4": "Forward 4",
    "b1": "Back 1",
    "b2": "Back 2",
    "l1": "Rotate Left 60°",
    "l2": "Rotate Left 120°",
    "r1": "Rotate Right 60°",
    "r2": "Rotate Right 120°",
    "r3": "Rotate 180°",
    "j2": "Jump 2",
    "j3": "Jump 3",
    "laser": "Laser",
    "laser_f1": "Forward 1 + Laser",
    "twolaser": "Double Laser",
    "threelaser": "Triple Laser",
    "triplelaser": "Triple Spread Laser",
    "hexalaser": "Hexa Spread Laser",
    "shield": "Shield",
    "f1_shield": "Forward 1 + Shield",
    "laser_shield": "Laser + Shield",
}

INITIAL_CARDS = tuple(
    ["r1"] * 3
    + ["l1"] * 3
    + ["r2"] * 1
    + ["l2"] * 1
    + ["r3"] * 1
    + ["f1"] * 5
    + ["f2"] * 3
    + ["f3"] * 1
    + ["b1"] * 2
    + ["laser"] * 5
)

OTHER_CARDS = (
    "f4",
    "b2",
    "shield",
    "laser_f1",
    "twolaser",
    "j2",
    "triplelaser",
    "f1_shield",
    "laser_shield",
    "threelaser",
    "j3",
    "hexalaser",
)

CATEGORIES = (
    {"f1", "f2", "f3", "f4"},
    {"l1", "l2", "r1", "r2", "r3"},
    {"b1", "b2"},
    {"j2", "j3"},
    {c for c in CARDS if "shield" in c},
    {c for c in CARDS if "laser" in c},
)


def insert_random(list_, element):
    list_.insert(random.randrange(len(list_) + 1), element)


def new_card_options(level):
    options = OTHER_CARDS[: level + 3]
    cards = None
    while cards is None or any(len(c.intersection(cards)) > 1 for c in CATEGORIES):
        cards = random.sample(options, k=2) + [random.choice(list(set(INITIAL_CARDS)))]
    return cards


def label(card, font, front_col, back_col=None):
    label = font.render(CARDS[card], False, front_col)
    new_label = pygame.Surface(
        (label.get_width() + 2, label.get_height() + 2), pygame.SRCALPHA
    )
    if back_col is not None:
        new_label.fill(back_col)
    new_label.blit(label, (1, 1))
    return new_label


class Deck:
    queue_size = 3
    left_zone = 2

    def __init__(self, card_images, animations):
        self.cards = list(INITIAL_CARDS)
        self.card_images = card_images
        self.animations = animations

        self.deal(ensure_nice=True)
        self.border = 2
        self.surface = pygame.Surface(
            (
                (14 + self.border) * (len(self.hand) + self.left_zone) - self.border,
                16 * 2 + self.border,
            ),
            pygame.SRCALPHA,
        )
        self.hover = None

    def redeal_without(self, removes):
        self.cards = self.all_cards
        for card in removes:
            self.cards.remove(card)
        self.deal()

    def deal(self, ensure_nice=False):
        random.shuffle(self.cards)
        while True:
            self.hand = [self.cards.pop() for _ in range(7)]
            if not ensure_nice or (
                all(sum(x == c for x in self.hand) >= 1 for c in ("f1", "r1", "l1"))
                and sum(x[0] == "f" for x in self.hand) >= 2
            ):
                break
            self.cards += self.hand
            random.shuffle(self.cards)
        self.discards = [self.cards.pop() for _ in range(4)]
        self.queue = [None for _ in range(self.queue_size)]

    @property
    def deck_pos(self):
        return ((14 + self.border) * (len(self.hand) - 1 + self.left_zone), 0)

    def play(self, index):
        self.animations.play_card("played", self.card_pos(index))
        self.animations.draw_card(index, self.deck_pos, duration=0.3)
        self.animations.slide_queue(None, 14 + self.border)
        self.queue.append(self.hand[index])
        card = self.queue.pop(0)
        if card is not None:
            self.animations.use_card(card)
            insert_random(self.discards, card)
            insert_random(self.cards, self.discards.pop())
        self.hand[index] = self.cards.pop()
        return card

    def card_pos(self, index):
        return ((index + self.left_zone) * (14 + self.border), 16 + self.border)

    def draw(self):
        self.surface.fill((0, 0, 0, 0))

        for i in reversed(range(3)):
            self.surface.blit(
                self.card_images["back"], (self.deck_pos[0] - i, self.deck_pos[1])
            )

        pygame.draw.rect(
            self.surface,
            colours.DARK_BACK_COL,
            (
                (14 + self.border) * self.left_zone,
                0,
                (14 + self.border) * self.queue_size - self.border,
                16,
            ),
        )
        acting_card, draw_pos = self.animations.acting_card(
            (1 * (14 + self.border) + self.animations.queue_shift(), 0),
            (0 * (14 + self.border) + self.animations.queue_shift(), 0),
            self.card_images,
        )
        if acting_card:
            self.surface.blit(acting_card, draw_pos)

        for i, c in enumerate(self.queue):
            if c is None:
                continue
            draw_pos = (
                (i + self.left_zone) * (14 + self.border)
                + self.animations.queue_shift(),
                0,
            )
            if i == len(self.queue) - 1:
                draw_pos = self.animations.played_pos(draw_pos)

            self.surface.blit(self.card_images[c], draw_pos)

        for i, c in reversed(list(enumerate(self.hand))):
            draw_pos = self.card_pos(i)
            sprite, draw_pos = self.animations.card_position(
                i, draw_pos, self.card_images[c], self.card_images["back"]
            )
            if draw_pos is None:
                continue
            self.surface.blit(sprite, draw_pos)
            if i == self.hover:
                self.surface.blit(self.card_images["hover_card"], draw_pos)

        sprite, draw_pos = self.animations.card_add_position(
            self.deck_pos, self.card_images
        )
        if sprite is not None:
            self.surface.blit(sprite, draw_pos)

        return self.surface

    def hovering(self, mp):
        self.hover = None
        for i in range(len(self.hand)):
            if self.animations.card_not_arrived(i):
                continue
            rel_pos = (
                mp[0] - (i + self.left_zone) * (14 + self.border),
                mp[1] - (16 + self.border),
            )
            try:
                if self.card_images["f1"].get_at(rel_pos)[3]:
                    self.hover = i
                    break
            except IndexError:
                continue
        return self.hover

    @property
    def all_cards(self):
        return sorted(
            self.discards + self.cards + self.hand + self.queue,
            key=list(CARDS.keys()).index,
        )

    def __len__(self):
        return len(self.all_cards)
