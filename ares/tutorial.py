import pygame

from . import colours, game

TEXT = (
    (
        "Your robot, ‘Ares’, has been falsely (he says!) accused of stealing the gold. "
        "The only escape is back down the mine."
    ),
    (
        "Ares starts the first level near the top-left corner of the map. "
        "You need to reach the downwards steps in the opposite corner before the cops arrive."
    ),
    "When the timer in the top-right of the screen runs out, cops will enter the level.",
    (
        "Along the bottom of the screen are your robot control cards. "
        "Click a card to send the instruction to your robot."
    ),
    "Ares has white lights on the front, and red lights on the back.",
    (
        "Mars is a long way away, so it takes three turns to see the results of your actions. "
        "Future instructions appear in the dark rectangle below."
    ),
    "You can hover over any card in your hand or tile on the map to see what it is.",
    (
        "The map mainly consists of empty cells (dirt), holes, rocks and enemies. "
        "Rocks and enemies can be destroyed with your laser."
    ),
    (
        "You can also find chests, which will contain useful parts. "
        "These get better as you go deeper."
    ),
    (
        "If an enemy hits you, bad cards will be added to your deck. "
        "You can get rid of some of these, or other weak cards, between levels."
    ),
    "If you take too much damage, it is a good idea just to restart (press escape).",
    "Ares need to descend nine levels to escape.",
)


class Tutorial(game.Game):
    def setup(self):
        super().setup()
        self.text_row = 0

    def draw(self):
        super().draw()
        if self.text_row >= len(TEXT):
            return
        words = [
            game.scale_up(
                self.FONT.render(word, False, colours.CHOOSE_TEXT_COL), self.scale / 2
            )
            for word in TEXT[self.text_row].split(" ")
        ]
        for word in words:
            word.set_alpha(200)
        initial_x = self.S.get_width() - (
            self.S.get_width() - self.hand_draw_pos[0]
        ) * len(self.deck.hand) / (len(self.deck.hand) + self.deck.left_zone)
        initial_y = 2 * self.scale
        x = initial_x
        y = initial_y
        for word in words:
            if (
                x + word.get_width()
                > self.S.get_width()
                - self.level_im.get_width() * self.scale
                - self.scale
            ):
                x = initial_x
                y += 11 * self.scale / 2
            self.S.blit(word, (x, y))
            x += word.get_width() + 6 * self.scale / 2

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONUP and self.hovered_card is None:
            self.text_row += 1
        return super().handle_event(event)
