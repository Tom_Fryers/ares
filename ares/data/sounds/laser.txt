{
  "oldParams": true,
  "wave_type": 0,
  "p_env_attack": 0,
  "p_env_sustain": 0.429,
  "p_env_punch": 0,
  "p_env_decay": 0.08372430331459287,
  "p_base_freq": 0.7009259071716025,
  "p_freq_limit": 0.48830684848677103,
  "p_freq_ramp": -0.21566665025762774,
  "p_freq_dramp": 0,
  "p_vib_strength": 0,
  "p_vib_speed": 0,
  "p_arp_mod": 0,
  "p_arp_speed": 0,
  "p_duty": 0.7661174243058781,
  "p_duty_ramp": -0.23058213598905317,
  "p_repeat_speed": 0,
  "p_pha_offset": 0,
  "p_pha_ramp": 0,
  "p_lpf_freq": 1,
  "p_lpf_ramp": 0,
  "p_lpf_resonance": 0,
  "p_hpf_freq": 0.27263734297731396,
  "p_hpf_ramp": 0,
  "sound_vol": 0.25,
  "sample_rate": 44100,
  "sample_size": 8
}