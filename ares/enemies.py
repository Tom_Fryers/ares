import random

from . import hexes


class Entity:
    def __init__(self, sprite, position, animations):
        if sprite is not None:
            self.sprite = sprite
        self.position = position
        self.animations = animations

    def draw(self, surface, origin_pos):
        draw_pos = self.animations.current_position(self).cartesian(3)
        draw_pos = (origin_pos[0] + draw_pos[0], origin_pos[1] + draw_pos[1])
        surface.blit(self.animations.current_sprite(self), draw_pos)

    @property
    def name(self):
        return self.__class__.__name__


class RotatableEntity(Entity):
    def __init__(self, sprites, position, rotation, animations):
        self.sprites = sprites
        self.rotation = rotation
        super().__init__(None, position, animations)

    @property
    def sprite(self):
        return self.sprites[self.rotation]

    def fire_beam(self, world, angle=None, position=None):
        if angle is None:
            angle = self.rotation
        if position is None:
            position = self.position
        beam_pos = position + hexes.DIRECTIONS[angle]
        will_destroy = None
        returning = None
        while abs(beam_pos) <= world.size:
            if entity := world.entity_on_cell(beam_pos):
                returning = entity
                break
            if world.tiles[beam_pos] == "rock":
                will_destroy = (beam_pos, "rock")
                world.tiles[beam_pos] = "dirt"
                break
            beam_pos += hexes.DIRECTIONS[angle]
        else:
            beam_pos = hexes.VecF(beam_pos) - hexes.VecF(hexes.DIRECTIONS[angle]) / 2
        self.animations.create_beam(position, beam_pos)
        if will_destroy is not None:
            self.animations.fade_tile(*will_destroy)
        return returning

    @property
    def in_front(self):
        return self.position + hexes.DIRECTIONS[self.rotation]

    @property
    def behind(self):
        return self.position - hexes.DIRECTIONS[self.rotation]


class Critter(Entity):
    def move(self, world):
        if abs(world.player.position - self.position) == 1:
            self.animations.bounce(self, world.player.position)
            world.kill(self)
            self.animations.damage("blank")
            return ["blank"]
        if random.random() < 0.5:
            return
        possible_directions = [
            x
            for x in hexes.DIRECTIONS
            if world.can_enter(self.position + x)
            and world.tiles[self.position + x] != "hole"
        ]
        if possible_directions:
            self.animations.translate(self, self.position)
            self.position += random.choice(possible_directions)


class Bat(Entity):
    def move(self, world):
        if abs(world.player.position - self.position) == 1:
            self.animations.bounce(self, world.player.position)
            world.kill(self)
            self.animations.damage("blank")
            return ["blank"]
        possible_directions = [
            x for x in hexes.DIRECTIONS if world.can_enter(self.position + x)
        ]
        min_dist = min(
            abs(world.player.position - (self.position + x))
            for x in possible_directions
        )
        possible_directions = [
            x
            for x in possible_directions
            if abs(world.player.position - (self.position + x)) == min_dist
        ]
        if possible_directions:
            self.animations.translate(self, self.position)
            self.position += random.choice(possible_directions)


class Ghost(Entity):
    def move(self, world):
        if abs(world.player.position - self.position) == 1:
            self.animations.bounce(self, world.player.position)
            world.kill(self)
            self.animations.damage("random")
            return ["random"]
        if random.random() < 0.5:
            return
        possible_directions = [
            x
            for x in hexes.DIRECTIONS
            if world.can_enter(self.position + x, ignore_rock=True)
        ]
        if possible_directions:
            self.animations.translate(self, self.position)
            self.position += random.choice(possible_directions)


class Bull(RotatableEntity):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.charge = False

    def move(self, world):
        if self.charge:
            if world.can_enter(self.in_front):
                self.animations.translate(self, self.position, duration=0.3)
                self.position = self.in_front
                world.kill_in_hole(self)
            else:
                self.charge = False
                self.animations.bounce(self, self.in_front)
                if world.tiles[self.in_front] == "rock":
                    self.animations.fade_tile(self.in_front, "rock")
                    world.tiles[self.in_front] = "dirt"
                elif abs(world.player.position - self.position) == 1:
                    self.animations.damage("blank")
                    self.animations.damage("blank")
                    return ["blank", "blank"]
                elif entity := world.entity_on_cell(self.in_front):
                    world.kill(entity)
        else:
            to_player = world.player.position - self.position
            if 0 in to_player.to_tuple():
                desired_rotation = hexes.DIRECTIONS.index(to_player / abs(to_player))
                if self.rotation == desired_rotation:
                    self.charge = True
                    self.move(world)
                    return

            if random.random() <= 0.4:
                self.animations.rotate(self, self.rotation)
                self.rotation += random.choice((0, 0, -1, 1))
                self.rotation %= 6


class Cop(RotatableEntity):
    def move(self, world):
        to_player = world.player.position - self.position
        if 0 in to_player.to_tuple():
            desired_rotation = hexes.DIRECTIONS.index(to_player / abs(to_player))
            if self.rotation == desired_rotation:
                entity = self.fire_beam(world)
                if entity is None:
                    return
                if entity is world.player:
                    self.animations.damage("random")
                    return ["random"]
                world.kill(entity)

            else:
                self.rotation = desired_rotation
            return
        if (
            random.random() <= 0.3
            or not world.can_enter(self.in_front)
            or world.tiles[self.in_front] == "hole"
        ):
            self.animations.rotate(self, self.rotation)
            self.rotation += random.choice((-1, -1, 1, 1, -2, 2, 3))
            self.rotation %= 6
        else:
            self.animations.translate(self, self.position, duration=0.3)
            self.position = self.in_front
